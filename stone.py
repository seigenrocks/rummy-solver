
import enum

import util


class Color(enum.Enum):
    R = 0
    Y = 1
    B = 2
    K = 3

    def __str__(self):
        return self.name

    def __eq__(self, other):
        return self.value == other.value

    def __ne__(self, other):
        return self.value == other.value


class Digit(enum.Enum):
    i = 0
    ii = 1
    iii = 2
    iv = 3
    v = 4
    vi = 5
    vii = 6
    viii = 7
    ix = 8
    x = 9
    xi = 10
    xii = 11
    xiii = 12

    def __str__(self):
        return self.name

    def __eq__(self, other):
        return self.value == other.value

    def __ne__(self, other):
        return self.value != other.value


n_colors = 4
n_digits = 13


class Stone(object):
    def __init__(self, color, digit):
        assert isinstance(color, Color)
        assert isinstance(digit, Digit)
        self._color = color.value
        self._digit = digit.value

    @property
    def color(self):
        return Color(self._color)

    @property
    def digit(self):
        return Digit(self._digit)

    def strait_adj(self):
        d = self._digit
        color_c = self.color
        strait_digits = [Digit(x-1) for x in [d] if x != 0] \
                        + [Digit(x+1) for x in [d] if d != 12]
        strait_stones = [Stone(color_c, d) for d in strait_digits]
        return strait_stones

    def col_adj(self):
        c = self._color
        digit_d = self.digit
        same_colors = [Color(x) for x in range(4) if x != c]
        same_stones = [Stone(c, digit_d) for c in same_colors]
        return same_stones

    def adj(self):
        return self.strait_adj() + self.col_adj()

    def is_adj(self, other):
        return other in self.adj()
    
    def __repr__(self):
        str_c = str(self.color)
        str_d = str(self.digit)
        return "(" + str_c + ", " + str_d + ")"

    def __hash__(self):
        return hash((self._color, self._digit))
    
    def __eq__(self, other):
        return (self._color == other._color) and (self._digit == other._digit)

    def __ne__(self, other):
        return (self._color != other._color) or (self._digit != other._digit)

    def __gt__(self, other):
        s_val = self._color * n_digits + self._digit
        o_val = other._color * n_digits + other._digit
        return s_val > o_val

    def __lt__(self, other):
        s_val = self._color * n_digits + self._digit
        o_val = other._color * n_digits + other._digit
        return s_val < o_val

    def __ge__(self, other):
        return self > other or self == other

    def __le__(self, other):
        return self < other or self == other


def segregate_colors(stones):
    return [[s for s in stones if s._color == x] for x in range(n_colors)]

def segregate_digits(stones):
    return [[s for s in stones if s._digit == x] for x in range(n_digits)]
