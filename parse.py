
import enum
import re

import plex

import stone


class TokenType(enum.Enum):
    COLOR = 0
    NUMBER = 1
    RANGE = 2
    TRIP = 3
    QUAD = 4


class Token(object):
    def __init__(self, tokentype, info):
        self.type = tokentype
        self.info = info


class StoneScanner(plex.Scanner):

    _range_re = re.compile("([0-9]+)-([0-9]+)")
    _triplet_re = re.compile("3([RrYyBbKk])")

    _space = plex.Any(" \t\n")
    _dash = plex.Str("-")
    _color = plex.Any("RrYyBbKk")
    _colorline = plex.Bol + _color
    _digit = plex.Range("09")
    _number = plex.Rep1(_digit)
    _range = _number + _dash + _number
    _triplet = plex.Bol + plex.Str("3") + _color
    _quadruplet = plex.Bol + plex.Str("4")

    def scan_color(self, text):
        return Token(TokenType.COLOR, text.upper())

    def scan_number(self, text):
        return Token(TokenType.NUMBER, int(text))

    def scan_range(self, text):
        m = self._range_re.match(text)
        assert m is not None
        start, end = (int(m.group(1)), int(m.group(2)))
        return Token(TokenType.RANGE, (start, end))

    def scan_triplet(self, text):
        m = self._triplet_re.match(text)
        assert m is not None
        color = m.group(1)
        return Token(TokenType.TRIP, color.upper())

    lexicon = plex.Lexicon([
        (_space, plex.IGNORE),
        (_colorline, scan_color),
        (_triplet, scan_triplet),
        (_quadruplet, Token(TokenType.QUAD, ())),
        (_range, scan_range),
        (_number, scan_number),
        ])

    def __init__(self, fp, name):
        super().__init__(self.lexicon, fp, name)


def gen_stone(colorinfo, digitinfo):
    color = getattr(stone.Color, colorinfo)
    digit = stone.Digit(digitinfo - 1)
    return stone.Stone(color, digit)


class ParseMode(enum.Enum):
    READY = 0
    COLORLINE_MORE = 1
    COLORLINE_READY = 2
    TRIPLET_MORE = 3
    TRIPLET_DONE = 4
    QUAD_MORE = 5
    QUAD_DONE = 6


class StoneParser(object):
    _readymap = {
            TokenType.COLOR: ParseMode.COLORLINE_MORE,
            TokenType.TRIP: ParseMode.TRIPLET_MORE,
            TokenType.QUAD: ParseMode.QUAD_MORE
            }

    def __init__(self):
        self._mode = ParseMode.READY
        self._colorstate = None

    def _transition_state(self, tokentype):
        mode = self._mode
        if mode in [ParseMode.READY,
                    ParseMode.TRIPLET_DONE,
                    ParseMode.QUAD_DONE]:
            try:
                new_mode = self._readymap[tokentype]
            except KeyError:
                raise RuntimeError("Expected colorline, triplet, or quad")
            self._colorstate = None
            self._mode = new_mode
        elif mode is ParseMode.COLORLINE_MORE:
            if tokentype not in [TokenType.NUMBER, TokenType.RANGE]:
                raise RuntimeError("Expected digit or range")
            self._mode = ParseMode.COLORLINE_READY
        elif mode is ParseMode.COLORLINE_READY:
            if tokentype in [TokenType.NUMBER, TokenType.RANGE]:
                pass
            else:
                try:
                    new_mode = self._readymap[tokentype]
                except KeyError:
                    raise RuntimeError("Expected anything, got something else")
                self._mode = new_mode
        elif mode is ParseMode.TRIPLET_MORE:
            if tokentype is TokenType.NUMBER:
                self._mode = ParseMode.TRIPLET_DONE
            else:
                raise RuntimeError("Expected number")
        elif mode is ParseMode.QUAD_MORE:
            if tokentype is TokenType.NUMBER:
                self._mode = ParseMode.QUAD_DONE
            else:
                raise RuntimeError("Expected number")
        else:
            raise RuntimeError("Unkown parser mode")

    def _consume_token(self):
        token = self._scanner.read()
        if token[0] is None:
            return None
        t = token[0].type
        info = token[0].info
        self._transition_state(t)
        if t is TokenType.COLOR:
            self._colorstate = info
        elif t is TokenType.RANGE:
            assert self._mode in [ParseMode.COLORLINE_MORE,
                                  ParseMode.COLORLINE_READY]
            stones = []
            for i in range(info[0], info[1] + 1):
                new_stone = gen_stone(self._colorstate, i)
                stones.append(new_stone)
            return stones
        elif t is TokenType.NUMBER:
            if self._mode is ParseMode.COLORLINE_MORE:
                new_stone = gen_stone(self._colorstate, info)
                return [new_stone]
            elif self._mode is ParseMode.COLORLINE_READY:
                new_stone = gen_stone(self._colorstate, info)
                return [new_stone]
            elif self._mode is ParseMode.TRIPLET_DONE:
                stones = []
                for i in range(stone.n_colors):
                    colorname = stone.Color(i).name
                    if colorname == self._colorstate:
                        continue
                    new_stone = gen_stone(colorname, info)
                    stones.append(new_stone)
                return stones
            elif self._mode is ParseMode.QUAD_DONE:
                stones = []
                for i in range(stone.n_colors):
                    new_stone = gen_stone(stone.Color(i).name, info)
                    stones.append(new_stone)
                return stones
            else:
                raise RuntimeError("Unexpected digit")
        elif t is TokenType.TRIP:
            self._colorstate = info
        elif t is TokenType.QUAD:
            self._colorstate = None
        return []

    def stones(self, fp, name):
        self._scanner = StoneScanner(fp, name)
        while True:
            maybe_stones = self._consume_token()
            if maybe_stones is None:
                return
            for s in maybe_stones:
                yield s


if __name__ == "__main__":
    import sys
    fnm = sys.argv[1]
    with open(fnm, "r") as fp:
        parser = StoneParser()
        for s in parser.stones(fp, fnm):
            print(s)
