
import copy
import collections
import enum
import itertools


def merge(change_me, take_my_key_val_pairs):
    '''In-place merge one map with the other.

:param merge_me: Map that gets the additional key-value pairs of the other.
:param take_my_key_val_pairs: Map from which key-value pairs are copied.

Returns nothing. May assert if type constraints are broken.'''
    assert isinstance(change_me, collections.MutableMapping)
    assert isinstance(take_my_key_val_pairs, collections.Mapping)
    for key, val in take_my_key_val_pairs.items():
        change_me[key] = val

def merged(source_A, source_B):
    '''Merge the two passed maps, keeping both unchanged.

:param source_A: One map to merge.
:param source_B: Another map to merge. Key-value pairs from this map take precedence.

Returns new dict with merged key-value pairs (shallow copied).
May assert if type constraints are broken.'''
    assert isinstance(source_A, collections.Mapping)
    assert isinstance(source_B, collections.Mapping)
    copied = dict(copy.copy(source_A))
    merge(copied, source_B)
    return copied

#https://docs.python.org/3/library/itertools.html
def powerset(iterable, minlen=0):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return itertools.chain.from_iterable(itertools.combinations(s, r)
                                         for r in range(minlen, len(s)+1))

def adjacents(iterable, n=2):
    itr = iter(iterable)
    iters = itertools.tee(itr, n)
    for index, crnt in enumerate(iters):
        for _ in range(index):
            next(crnt)
    return zip(*iters)
