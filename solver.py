
import collections
import copy

import sets
import stone
import util


class StoneSet(object):
    def __init__(self):
        self.count = 0
        self.sets = []

    def __add__(self, other):
        newset = copy.deepcopy(self)
        if type(other) is int:
            newset.count += other
        elif type(other) is StoneSet:
            newset.count += other.count
            newset.sets += other.sets
        elif type(other) is list:
            newset.sets += other
        else:
            raise TypeError("Expected int, list or StoneSet: %r" % other)
        return newset

    def __sub__(self, other):
        if type(other) is not int:
            raise TypeError("Expected int. Found: %r" % other)
        newset = copy.deepcopy(self)
        newset.count = self.count - other
        return newset

    def __repr__(self):
        return str((self.count, self.sets))


class SetPossibilities(object):
    def __init__(self):
        self._mapping = collections.defaultdict(StoneSet)

    def __getitem__(self, stone):
        return self._mapping[stone]

    def __setitem__(self, stone, val):
        if type(val) is int:
            self._mapping[stone].count = val
        elif type(val) is StoneSet:
            self._mapping[stone] = val
        else:
            raise TypeError("Expected int or StoneSet. Found: %r" % val)

    def __delitem__(self, stone):
        self._mapping[stone] -= 1
        if self._mapping[stone].count == 0:
            others = set()
            for s in self[stone].sets:
                others.update([x for x in s.stones if x != stone])
            for o in others:
                filtered = [x for x in self[o].sets if stone not in x.stones]
                self._mapping[o].sets = filtered
            del self._mapping[stone]

    def show(self):
        stones = list(self.keys())
        stones.sort()
        ret = ""
        for s in stones:
            stoneset = self[s]
            count, sets = stoneset.count, stoneset.sets
            local = str(s) + " x" + str(count) + ": "
            indent = len(local)
            for idx, ss in enumerate(sets):
                local += str(ss) + "\n"
                if idx != len(sets) - 1:
                    local += " "*indent
            ret += local

    def keys(self):
        return self._mapping.keys()

    def values(self):
        return self._mapping.values()

    def items(self):
        return self._mapping.items()

    def leastsets(self):
        ret = None
        leastsets = float("inf")
        leastcount = float("inf")
        for s, stoneset in self._mapping.items():
            n_sets = len(stoneset.sets)
            count = stoneset.count
            if count < leastcount and n_sets < leastsets:
                leastsets = n_sets
                leastcount = count
                ret = (s, stoneset)
        return ret


class SolutionNode(object):
    def __init__(self, parent, sp, solsets):
        self._sp = sp
        self._solsets = solsets
        self._children = []
        self._parent = parent
        self._solution = False

    @property
    def children(self):
        return self._children

    @property
    def parent(self):
        return self._parent

    @property
    def siblings(self):
        parent = self._parent
        if parent is None:
            return []
        else:
            return parent.children

    def show(self):
        ret = ""
        if not self.is_solution():
            ret += self._sp.show()
        self._solsets.sort()
        for s in self._solsets:
            ret += str(s) + "\n"
        return ret

    def is_solution(self):
        return self._solution

    def gen_children(self):
        ret = self._sp.leastsets()
        if ret is None:
            self._solution = True
            return False
        stone, stoneset = ret
        if len(stoneset.sets) == 0:
            return False
        for s in stoneset.sets:
            newsp = copy.deepcopy(self._sp)
            for setstone in s.stones:
                del newsp[setstone]
            newsolsets = self._solsets + [s]
            newsolnode = SolutionNode(self, newsp, newsolsets)
            self._children.append(newsolnode)
        return True

    def depthfirst_solutions(self):
        solutions = []
        todo = [self]
        while len(todo) > 0:
            node = todo[0]
            del todo[0]
            todo += node.children
            if node.is_solution():
                solutions.append(node)
        return solutions


class SetFinder(object):
    def __init__(self, stones):
        self._sp = SetPossibilities()
        for s in stones:
            self._sp[s] += 1
        self._populate_possibilities()
        self._soltree = SolutionNode(None, self._sp, [])

    def _storeall(self, stones):
        possibility = sets.Set(list(stones))
        for s in stones:
            self._sp[s] += [possibility]

    def _store_set_adjacents(self, stones):
        for setlen in range(3, 1 + len(stones)):
            for subset in util.adjacents(stones, n=setlen):
                self._storeall(subset)

    def _populate_from_colors(self, stones):
        colors = stone.segregate_colors(stones)
        runlen = 1
        for color in colors:
            if len(color) < 3:
                continue
            color.sort()
            prev_stone = None
            for index, s in enumerate(color):
                if prev_stone is not None and s.is_adj(prev_stone):
                    runlen += 1
                else:
                    self._store_set_adjacents(color[index-runlen:index])
                    runlen = 1
                    prev_stone = None
                prev_stone = s
            self._store_set_adjacents(color[-runlen:])

    def _populate_from_digits(self, stones):
        digits = stone.segregate_digits(stones)
        for digit in digits:
            if len(digit) >= 3:
                assert len(digit) < 5
                for subset in util.powerset(digit, minlen=3):
                    self._storeall(subset)

    def _populate_possibilities(self):
        stones = list(self._sp.keys())
        self._populate_from_colors(stones)
        self._populate_from_digits(stones)

    def _solrecurse(self, node):
        todo = [node]
        while len(todo) > 0:
            node = todo[0]
            del todo[0]
            node.gen_children()
            todo += node.children

    def solve(self):
        self._solrecurse(self._soltree)
        return self._soltree.depthfirst_solutions()
