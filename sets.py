
import stone


class Set(object):
    def __init__(self, stones):
        assert len(stones) >= 3
        self._check_dups(stones)
        stones.sort()
        self._strait = not self._is_col_set(stones)
        assert (not self._strait) or self._is_strait(stones)
        self._stones = stones

    @property
    def strait(self):
        return self._strait

    @property
    def same(self):
        return not self._strait

    @property
    def stones(self):
        return self._stones

    @staticmethod
    def _check_dups(stones):
        for index, s in enumerate(stones):
            other_stones = stones[:index] + stones[index+1:]
            assert s not in other_stones

    @staticmethod
    def _is_col_set(stones):
        if len(stones) > 4:
            return False
        fst = stones[0]
        for s in stones[1:]:
            if fst.color == s.color:
                return False
            if fst.digit != s.digit:
                return False
        return True

    @staticmethod
    def _is_strait(stones):
        for index, s in enumerate(stones[:-1]):
            adj = s.strait_adj()
            found_adj = False
            for adj_stone in adj:
                try:
                    stones[index:].index(adj_stone)
                except ValueError:
                    pass
                else:
                    found_adj = True
                    break
            if not found_adj:
                return False
        return True

    def _triplet_color_excluded(self):
        colors_included = []
        for s in self._stones:
            cval = s.color.value
            colors_included.append(cval)
        color_excluded = [x for x in [y for y in range(4)]
                          if x not in colors_included]
        assert len(color_excluded) == 1
        return color_excluded[0]

    def __repr__(self):
        if self._strait:
            color = self._stones[0].color
            lo_num = 1 + self._stones[0].digit.value
            hi_num = 1 + self._stones[-1].digit.value
            ret = str(color) + " " + str(lo_num) + "-" + str(hi_num)
        elif len(self._stones) == 3:
            c = self._triplet_color_excluded()
            color_excluded_name = stone.Color(c).name
            num = 1 + self._stones[0].digit.value
            ret = "3" + color_excluded_name + " " + str(num)
        elif len(self._stones) == 4:
            num = 1 + self._stones[0].digit.value
            ret = "4 " + str(num)
        else:
            assert False
        return ret

    def _val(self):
        if self._strait:
            lo_val, hi_val = [self._stones[x].digit.value for x in [0, -1]]
            col = self._stones[0].color.value
            ret = col*(stone.n_digits)**2 + lo_val * stone.n_digits + hi_val
        elif len(self._stones) == 3:
            start = (stone.n_colors)*(stone.n_digits)**2
            col = self._triplet_color_excluded()
            ret = start + col*(stone.n_digits)**2 + self._stones[0].digit.value
        elif len(self._stones) == 4:
            start = (stone.n_digits)**3
            ret = start + self._stones[0].digit.value
        else:
            assert False
        return ret

    def __eq__(self, other):
        return self._val() == other._val()

    def __neq__(self, other):
        return self._val() != other._val()

    def __lt__(self, other):
        return self._val() < other._val()

    def __gt__(self, other):
        return self._val() > other._val()

    def __le__(self, other):
        return self._val() <= other._val()

    def __ge__(self, other):
        return self._val() >= other._val()
