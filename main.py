#!/usr/bin/env python3

import sys

import parse
import solver


def main(args):
    if len(args) != 2:
        print("usage: %s <filename>")
    fnm = args[1]
    if fnm == "-":
        fnm = "/dev/stdin"
    with open(fnm, "r") as fp:
        parser = parse.StoneParser()
        stones = [s for s in parser.stones(fp, fnm)]
    sf = solver.SetFinder(stones)
    solutions = sf.solve()
    for sol in solutions:
        print(sol.show())


if __name__ == "__main__":
    main(sys.argv)
